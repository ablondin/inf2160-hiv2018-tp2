# Travail pratique 2 - Déplacement d'un agent intelligent

## Description

Décrire ici le projet de façon assez générale. Vous pouvez reprendre tels quels
des passages disponibles dans l'énoncé si vous voulez, mais n'hésitez pas à
l'adapter à votre propre style.

## Auteur

Indiquer ici l'auteur et son code permanent. Par exemple

Bob LeMineur (LEMB12345678)

## Fonctionnement

Expliquer comment faire fonctionner le projet, en particulier

- indiquer à l'utilisateur s'il doit installer certaines dépendances (dire
  lesquelles ou référer à la section), y compris SWI-Prolog;
- expliquer comment faire tourner le projet de façon interactive (à l'aide de
  l'interpréteur, avec au moins un exemple d'interaction, incluant la sortie);
- expliquer comment lancer les tests automatiques;

Utilisez bien le format Markdown pour mettre des extraits de commande, avec le
résultat attendu s'il y a lieu.

Donnez au moins un exemple d'un chemin obtenu pour un niveau donné différent de
ceux que j'ai fournis.

## Contenu du projet

Décrire *tous* les fichiers contenus dans le dépôt. Utilisez une liste à puces.

## Dépendances

Indiquer toutes les dépendances pour faire fonctionner ce projet. Dans chaque
cas, donnez un lien vers le site officiel de cette dépendance.

## Références

Indiquer ici vos références s'il y a lieu. Si vous n'en avez pas utilisé,
supprimez cette section.

## Statut

Indiquer si le projet est complété ou s'il y a eu des problèmes/limitations.
